import React from "react";
import { Route, HashRouter, NavLink } from "react-router-dom";
import ReactHello from "./components/reactHello";
import MyLayout from "./components/layout";
import MyModal from "./components/modal";

import './app.css'

const MyMenu = () => {
  return (
    <ul className="ulBox">
      <li>
        <NavLink to="/">首页</NavLink>
      </li>
      <li>
        <NavLink to="/layout">布局-web组件</NavLink>
      </li>
      <li>
        <NavLink to="/modal">弹窗-web组件</NavLink>
      </li>
      <li>
        <NavLink to="/reactHello">react组件</NavLink>
      </li>
    </ul>
  );
};

class App extends React.Component {
  render() {
    return (
      <HashRouter>
        <MyMenu />
        <div className="contentBox">
          <Route exact path="/" component={ReactHello} />
          <Route exact path="/layout" component={MyLayout} />
          <Route exact path="/modal" component={MyModal} />
          <Route exact path="/reactHello" component={ReactHello} />
        </div>
      </HashRouter>
    );
  }
}

export default App;
