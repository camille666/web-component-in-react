import React, { PureComponent } from "react";
import "./modal.css";
import "./modalWeb.js";

class MyModal extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      'visible': 'false'
    };
  }

  componentDidMount() {
    const self = this;
    const openBtn =document.getElementById('checkbox1');
    const myModalDom = document.querySelector('my-modal');
    
    myModalDom.onclick = function(e) {
      // 打开弹窗
      if (e.target.getAttribute('visible') === 'true') {
        openBtn.checked = 'checked'
      } else {
        openBtn.checked = ''
      }
    }

    // 需要在这里写监听，不能直接在dom上绑定
    myModalDom.oncancel = function(e) {
      self.handleCancel();
    }

    myModalDom.onsubmit = function(e) {
      self.handleSure();
    }

    // 打开弹窗
    openBtn.onclick = function(e) {
      if (e.target.checked) {
        self.setState({
          visible: 'true'
        });
      } else {
        self.setState({
          visible: 'false'
        });
      }
    }
  }

  // shadow dom 里面的回调
  handleSure() {
    console.log('确认');
    this.setState({
      visible: 'false'
    });
  }
  handleCancel () {
    console.log('取消');
    this.setState({
      visible: 'false'
    });
  }

  render() {
    return (
      <div>
        <div id="toggles">
          <input
            type="checkbox"
            name="checkbox1"
            id="checkbox1"
            className="toggleBtn"
          />
          <label
            htmlFor="checkbox1"
            className="checkboxLabel"
            data-off="关闭弹窗"
            data-on="打开弹窗"
          ></label>
        </div>
        <my-modal
          data-title="这是一个web组件modal框"
          controlmove="true"
          controlfullscreen="true"
          visible={this.state.visible}
        >
          <div slot="J_con">这是内容</div>
        </my-modal>
      </div>
    );
  }
}

export default MyModal;
