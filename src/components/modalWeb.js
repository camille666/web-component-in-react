class MyModal extends HTMLElement {
  // 监听属性值
  static get observedAttributes() {
    return ["visible", "ismove", "isfullscreen"];
  }

  constructor() {
    super();
  }

  // 移动
  dragSelf(dragObj, triggerObj, moveBtn, fn) {
    if ((!JSON.parse(this.getAttribute('ismove')) && !!fn) || (!!JSON.parse(this.getAttribute('ismove')) && !fn)) {
        
        fn && fn('正在进入移动模式····');
        this.setAttribute('ismove', 'true');
        triggerObj.style.cursor = 'move';
        moveBtn && (moveBtn.textContent = "退出移动");
        // 鼠标按下
        triggerObj.addEventListener('mousedown',function(e) {
            const event = e || window.event;
            
            const x = event.clientX - dragObj.offsetLeft; 
            const y = event.clientY - dragObj.offsetTop; 
            // 鼠标移动
            document.onmousemove = function(e){
                const event = e || window.event;
                let iL = event.clientX - x;
                let iT = event.clientY - y;
                const maxL = document.documentElement.clientWidth - dragObj.offsetWidth;
                const maxT =
                document.documentElement.clientHeight - dragObj.offsetHeight;
                iL <= 0 && (iL = 0);
                iT <= 0 && (iT = 0);
                iL >= maxL && (iL = maxL);
                iT >= maxT && (iT = maxT);
                dragObj.style.left = iL + "px";
                dragObj.style.top = iT + "px";
                return false;
            }
            // 鼠标抬起来
            document.onmouseup = function(){
                document.onmousemove = null;
                document.onmouseup = null;
                this.releasecapture && this.releasecapture();
            };
            this.setcapture && this.setcapture();
            return false;
        });
        
    } else  {
        fn && fn('正在退出移动模式····');
        this.resetIsmove(triggerObj, moveBtn);
    }  
  }

  // 全屏
  fullScreen(scaleObj, fullScreenBtn, fn) {
    if ((!JSON.parse(this.getAttribute('isfullscreen')) && !!fn) || (!!JSON.parse(this.getAttribute('isfullscreen')) && !fn)) {
        fn && fn('正在进入全屏模式····');
        // 设置弹框
        scaleObj.style.top = scaleObj.style.left = 0;
        scaleObj.style.marginLeft = 0;
        scaleObj.style.width = document.documentElement.clientWidth + "px";
        scaleObj.style.height = document.documentElement.clientHeight + "px";
        
        // 设置root属性
        this.setAttribute('isfullscreen', 'true');
        // 设置当前按钮
        fullScreenBtn && (fullScreenBtn.textContent = "退出全屏");
    } else {
        fn && fn('正在退出全屏模式····');
        this.resetIsFullScreen(scaleObj, fullScreenBtn);
    } 
  }
  
  // 更新样式 
  updateStyle() {
    if (this.wrapperBox) {
      if (!!JSON.parse(this.getAttribute("visible"))) {
        this.wrapperBox.style.display = "-webkit-box";
      } else {
        this.wrapperBox.style.display = "none";
      }  
    }
  }

  // 监听事件
  connectedCallback() {
    const shadowDom = this.attachShadow({ mode: "open" });

    // style
    const styleEle = document.createElement("style");
    styleEle.textContent = `
        .wrapper {
            position: fixed;
            left: 0;
            top: 0;
            z-index: 100;
            width: 100%;
            min-width: 375px;
            height: 100%;
            text-align: center;
            background-color: rgba(0,0,0,0.5);
            -webkit-box-orient: vertical;
            -webkit-box-pack: center;
            -moz-box-orient: vertical;
            -moz-box-pack: center;
            vertical-align: middle;
            display: -webkit-box;
            display: -moz-box;
        }
        .mask {
            position: fixed;
            left: 0;
            top: 0;
            z-index: 100;
            width: 100%;
            min-width: 375px;
            height: 100%;
            text-align: center;
            background-color: rgba(0,0,0,0.5);
            -webkit-box-orient: vertical;
            -webkit-box-pack: center;
            -moz-box-orient: vertical;
            -moz-box-pack: center;
            vertical-align: middle;
            display: -webkit-box;
            display: -moz-box;
        }
        .modalBox {
            position: relative;
            left: 50%;
            margin-left: -200px;
            z-index: 1000;
            width: 400px;
            height: 400px;
            background-color: #fff;
            background-clip: padding-box;
            border: 0;
            border-radius: 4px;
            -webkit-box-shadow: 0 4px 12px rgba(0,0,0,0.15);
            box-shadow: 0 4px 12px rgba(0,0,0,0.15);
            pointer-events: auto;
        }
        .title {
            padding: 10px;
            color: red;
            font-weight: bold;
        }
        .content {
            padding: 10px;
        }
        .btns {
            position: absolute;
            left: 0;
            bottom: 5px;
            width: 100%;
        }
        .moveBtn {
            display: inline-block;
            padding: 0 15px;
            margin-right: 10px;
            height: 32px;
            line-height: 32px;
            font-size: 14px;
            text-align: center;
            color: #fff;
            background-color: #1890ff;
            border:1px solid #1890ff;
            text-shadow: 0 -1px 0 rgba(0,0,0,0.12);
            box-shadow: 0 2px 0 rgba(0,0,0,0.045);
            cursor: pointer;
            transition: all .3s cubic-bezier(.645, .045, .355, 1);
            user-select: none;
            touch-action: manipulation;
            border-radius: 4px;
        }
        .fullScreenBtn {
            display: inline-block;
            padding: 0 15px;
            margin-right: 10px;
            height: 32px;
            line-height: 32px;
            font-size: 14px;
            text-align: center;
            color: #fff;
            background-color: #1890ff;
            border:1px solid #1890ff;
            text-shadow: 0 -1px 0 rgba(0,0,0,0.12);
            box-shadow: 0 2px 0 rgba(0,0,0,0.045);
            cursor: pointer;
            transition: all .3s cubic-bezier(.645, .045, .355, 1);
            user-select: none;
            touch-action: manipulation;
            border-radius: 4px;
        }
        .sureBtn {
            display: inline-block;
            padding: 0 15px;
            height: 32px;
            line-height: 32px;
            font-size: 14px;
            text-align: center;
            color: #fff;
            background-color: #1890ff;
            border:1px solid #1890ff;
            text-shadow: 0 -1px 0 rgba(0,0,0,0.12);
            box-shadow: 0 2px 0 rgba(0,0,0,0.045);
            cursor: pointer;
            transition: all .3s cubic-bezier(.645, .045, .355, 1);
            user-select: none;
            touch-action: manipulation;
            border-radius: 4px;
        }
        .cancelBtn {
            display: inline-block;
            margin-right: 10px;
            padding: 0 15px;
            height: 32px;
            line-height: 32px;
            font-size: 14px;
            text-align: center;
            box-shadow: 0 2px 0 rgba(0,0,0,0.015);
            cursor: pointer;
            transition: all .3s cubic-bezier(.645, .045, .355, 1);
            user-select: none;
            touch-action: manipulation;
            border-radius: 4px;
            color: rgba(0,0,0,0.65);
            background-color: #fff;
            border: 1px solid #d9d9d9;
        }`;

    shadowDom.appendChild(styleEle);

    // 外层div
    const wrapperDom = document.createElement("div");
    wrapperDom.classList.add("wrapper");
    this.wrapperBox = wrapperDom;

    // 外层div
    const maskDom = document.createElement("div");
    maskDom.classList.add("mask");

    // 弹窗
    const modalDom = document.createElement("div");
    modalDom.classList.add("modalBox");

    // header
    const titleDom = document.createElement("div");
    titleDom.classList.add("title");
    titleDom.textContent = this.getAttribute("data-title");

    // content
    const contentDom = document.createElement("div");
    contentDom.classList.add("content");
    contentDom.innerHTML = `<slot name="J_con"></slot>`;

    // 按钮
    const btnsDom = document.createElement("div");
    btnsDom.classList.add("btns");

    // 控制是否在窗口显示移动按钮
    if (JSON.parse(this.getAttribute("controlmove"))) {
        const moveBtn = document.createElement("div");
        moveBtn.classList.add("moveBtn");
        moveBtn.textContent = "进入移动";
        btnsDom.appendChild(moveBtn);
    }
    
    // 控制是否在窗口显示全屏按钮
    if (JSON.parse(this.getAttribute("controlfullscreen"))) {
        const fullScreenBtn = document.createElement("div");
        fullScreenBtn.classList.add("fullScreenBtn");
        fullScreenBtn.textContent = "进入全屏";
        btnsDom.appendChild(fullScreenBtn);
    }

    const cancelBtnDom = document.createElement("div");
    cancelBtnDom.classList.add("cancelBtn");
    cancelBtnDom.textContent = "取消";
    const sureBtnDom = document.createElement("div");
    sureBtnDom.classList.add("sureBtn");
    sureBtnDom.textContent = "确定";
    
    btnsDom.appendChild(cancelBtnDom);
    btnsDom.appendChild(sureBtnDom);

    modalDom.appendChild(titleDom);
    modalDom.appendChild(contentDom);
    modalDom.appendChild(btnsDom);
    wrapperDom.appendChild(modalDom);
    wrapperDom.appendChild(maskDom);
    shadowDom.appendChild(wrapperDom);
    console.dir(this);
    const moveBtn = this.shadowRoot.querySelector(".moveBtn");
    const fullScreenBtn = this.shadowRoot.querySelector(".fullScreenBtn");
    
    console.log("在dom添加弹窗");
    this.updateStyle();

    // 处理默认传值
    this.dragSelf(modalDom, titleDom, moveBtn);
    this.fullScreen(modalDom, fullScreenBtn);

    moveBtn && moveBtn.addEventListener(
        "click",
        event => {
          event.stopPropagation();
          this.dragSelf(modalDom, titleDom, event.target, (param) => {
            setTimeout(() => {alert(param)}, 100);
          });
        },
        false
      );
    fullScreenBtn && fullScreenBtn.addEventListener(
    "click",
    event => {
        event.stopPropagation();
          this.fullScreen(modalDom, event.target, (param) => {
            setTimeout(() => {alert(param)}, 100);
          });
    },
    false
    );
    
    sureBtnDom.addEventListener(
      "click",
      event => {
        event.stopPropagation();
        this.onsubmit();
        this.resetSate(modalDom, fullScreenBtn, titleDom, moveBtn);
      },
      false
    );
    cancelBtnDom.addEventListener(
      "click",
      event => {
        event.stopPropagation();
        this.oncancel();
        this.resetSate(modalDom, fullScreenBtn, titleDom, moveBtn);
      },
      false
    );
    maskDom.addEventListener(
      "click",
      () => {
        console.log(this);
        this.oncancel();
        this.resetSate(modalDom, fullScreenBtn, titleDom, moveBtn);
      },
      false
    );
  }

  disconnectedCallback() {
    console.log("从dom移除弹窗");
  }

  // 初始化移动
  resetIsmove(triggerObj, moveBtn) {
    this.setAttribute('ismove', 'false');
    triggerObj.style.cursor = '';
    moveBtn && (moveBtn.textContent = "进入移动");
  }

  // 初始化是否全屏
  resetIsFullScreen(scaleObj, fullScreenBtn) {
    this.setAttribute('isfullscreen', 'false');
    scaleObj.style.width = 400 + "px";
    scaleObj.style.height = 400 + "px";
    scaleObj.style.left =
       '50%';
        scaleObj.style.marginLeft = -200 + 'px';
    fullScreenBtn && (fullScreenBtn.textContent = "进入全屏");
  }

  resetSate(scaleObj, fullScreenBtn, triggerObj, moveBtn) {
    this.resetIsmove(triggerObj, moveBtn);
    this.resetIsFullScreen(scaleObj, fullScreenBtn);
  }

  attributeChangedCallback(name, oldValue, newValue) {
    console.log("元素属性发生改变", name, oldValue, newValue);
    this.updateStyle();
  }
}

customElements.define("my-modal", MyModal);
